# HTTP Debug ticks

## Force curl to resolve to localhost server
`curl --resolve mydomain:443:127.0.0.1 https://mydomain`

## Check how many simultaneous connections are currently active in Apache
`ss -t state established '( sport = :http or sport = :https )' | wc -l`
or
`netstat -an | grep ESTABLISHED | grep -E '(:80|:443)' | wc -l`
